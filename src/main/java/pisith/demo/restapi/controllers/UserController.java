package pisith.demo.restapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pisith.demo.restapi.models.Users;
import pisith.demo.restapi.services.UserService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired private UserService userService;

    @GetMapping("/")
    public String welcome() {
        return "Welcome to User API";
    }

    @GetMapping("/users")
    private List<Users> getAllUsers() {
        return this.userService.findAllUsers();
    }

    @GetMapping("/{id}")
    public Optional<Users> findUserById(@PathVariable String id) {
        return userService.findUserById(id);
    }

    @PostMapping
    public Users addNewUser(@RequestBody Users users) {
        return userService.addNewUser(users);
    }

    @PutMapping("/{id}")
    public Users updateUser(@PathVariable String id ,@RequestBody Users users) {
        return userService.updateUser(id, users);
    }
}
