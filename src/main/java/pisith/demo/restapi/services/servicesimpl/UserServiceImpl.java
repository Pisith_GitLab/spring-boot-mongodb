package pisith.demo.restapi.services.servicesimpl;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import pisith.demo.restapi.models.Users;
import pisith.demo.restapi.repositories.UserRepository;
import pisith.demo.restapi.services.UserService;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired private UserRepository userRepository;
    @Autowired private MongoTemplate mongoTemplate;


    @Override
    public List<Users> findAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public Optional<Users> findUserById(String id) {
        return userRepository.findById(id);
    }

    @Override
    public Users addNewUser(Users users) {
        users.set_id(ObjectId.get().toString());
        return userRepository.save(users);
    }

    @Override
    public Users updateUser(String id, Users users) {
        Optional<Users> getUser = userRepository.findById(id);
        Users userObj = getUser.get();
        userObj.set_id(id);
        userObj.setUsername(users.getUsername());
        return userRepository.save(userObj);
    }

}
