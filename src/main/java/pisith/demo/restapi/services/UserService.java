package pisith.demo.restapi.services;

import org.springframework.stereotype.Service;
import pisith.demo.restapi.models.Users;

import java.util.List;
import java.util.Optional;

@Service
public interface UserService {
    List<Users> findAllUsers();
    Optional<Users> findUserById(String id);
    Users addNewUser(Users users);
    Users updateUser(String id, Users users);
}
