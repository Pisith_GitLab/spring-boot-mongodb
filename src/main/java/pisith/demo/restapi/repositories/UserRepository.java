package pisith.demo.restapi.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pisith.demo.restapi.models.Users;

import java.util.List;

@Repository
public interface UserRepository extends MongoRepository<Users, String> {
}
