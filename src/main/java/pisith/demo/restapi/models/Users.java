package pisith.demo.restapi.models;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

//@Getter
//@Setter
//@ToString(exclude = {"_id"})
@Document(collection = "users")
public class Users {

    @Id
    private String _id;
    @Field(name = "username")
    private String username;

    public String get_id(){return _id;}
    public void set_id(String userId){this._id = userId;}
    public String getUsername(){return username;}
    public void setUsername(String username){this.username = username;}


}
